import 'package:http/http.dart' as http;
import 'package:anime_force/src/core/port/access.dart';

class HttpAnimeForceWebsite implements AnimeForceWebsite {
  final String baseUrl;
  http.Client _client;

  HttpAnimeForceWebsite (this.baseUrl);

  Future<String> fetchAnimeListPage() async {
    return _requestWithEmptyStringOnError(Uri.parse(this.baseUrl + 'lista-anime/'));
  }

  Future<String> fetchAnime(Uri url) async {
    return await _requestWithEmptyStringOnError(url);
  }

  Future<String> fetchStreamingPage(Uri url) async {
    return await _requestWithEmptyStringOnError(url);
  }

  Future<String> _requestWithEmptyStringOnError(Uri url) async {
    if (_client == null) {
      _client = http.Client();
    }
    try {
      http.Response response = await _client.get(url);
      return response.body;
    } catch (e) {
      print(e);
      return '';
    }
  }
}
