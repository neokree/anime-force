import 'package:anime_force/src/core/component/anime/domain/website.dart';
import 'package:anime_force/src/core/port/parsing.dart';
import 'package:html/dom.dart';
import 'package:html/parser.dart' as html;

class HtmlParser implements Parser {
  @override
  AnimeListPage parseAnimeListPage(String webPage) {
    Document document = html.parse(webPage);
    var theContent = document.getElementById('content');
    if (theContent == null) return AnimeListPage([]);

    var list = theContent.getElementsByClassName('item-wrapper').map((element) {
      var name = _parseName(element.text);

      // parse url
      Element tagA = element.getElementsByTagName('a').first;
      String hrefValue = tagA.attributes['href'];
      Uri url = Uri.parse(hrefValue);

      return AnimeLink(
        name: name,
        url: url,
      );
    }).toList();

    return AnimeListPage(list);
  }

  /// Removes from the anime name all SEO related values
  String _parseName(String rawName) {
    return rawName
        .replaceAll('Sub Ita', '')
        .replaceAll('Download & Streaming', '')
        .replaceAll('Streaming', '')
        .trim();
  }

  @override
  AnimePage parseAnimePage(String webPage, Uri websiteUrl) {
    Document document = html.parse(webPage);
    var body = document.body;
    try {
      String name = body.getElementsByTagName('h1').first.text;
      var theContent = body.getElementsByClassName('entry-content').first;
      var tables = theContent.getElementsByTagName('table');
      var contentTableBody = tables.first.children.first;
      var episodesTableBody = tables[1].children.first;

      Map<String, String> contents = Map();
      contentTableBody.getElementsByTagName('tr').forEach((e) {
        List<Element> tds = e.getElementsByTagName('td');
        assert(tds.length == 2);

        String section = tds[0].text;
        String content = tds[1].text;

        contents[section] = content;
      });

      List<String> genres = contents['Genere']
              ?.split(',')
              ?.map((genre) => genre.trim())
              ?.toList() ??
          [];

      String imageUrl =
          theContent.getElementsByTagName('img').first.attributes['src'];
      if (imageUrl.startsWith('\/\/')) {
        imageUrl = 'https:$imageUrl';
      }

      List<EpisodeLink> episodes = [];

      /// skipped the table header
      episodesTableBody.getElementsByTagName('tr').skip(1).forEach((e) {
        List<Element> tds = e.getElementsByTagName('td');

        String identifier = tds[0].text;
        String href = tds[1].getElementsByTagName('a').first.attributes['href'];
        var uri = Uri.parse(href);
        if (!uri.hasScheme) uri = uri.replace(scheme: websiteUrl.scheme);
        if (uri.host == '') uri = uri.replace(host: websiteUrl.host);

        bool downloadable =
            tds[1].getElementsByTagName('img').first.attributes['src'] !=
                '/DDL/nodownload.png';

        EpisodeLink episode = EpisodeLink(
          identifier: identifier,
          uri: uri,
          downloadable: downloadable,
        );
        episodes.add(episode);
      });

      return AnimePage(
        name: _parseName(name),
        yearOfPublication: contents['Anno'],
        plot: contents['Trama'],
        genre: genres,
        imageUrl: imageUrl,
        episodes: episodes,
      );
    } on StateError {
      return null;
    }
  }

  @override
  StreamingPage parseStreamingPage(String webPage) {
    Document document = html.parse(webPage);
    var sourceElements = document.body.getElementsByTagName('source');
    if (sourceElements.isEmpty) return null;

    var source = sourceElements.first.attributes['src'];
    return StreamingPage(Uri.parse(source));
  }
}
