/// All value objects used to connect with the website

import 'package:anime_force/extension/class.dart';
import 'package:meta/meta.dart';

class AnimeListPage {
  final List<AnimeLink> animeList;

  AnimeListPage(this.animeList) : assert(animeList != null);
}

class AnimeLink extends Equatable {
  final String name;
  final Uri url;

  AnimeLink({
    @required this.name,
    @required this.url,
  });

  @override
  List<Object> get props => [name, url];
}

class AnimePage extends Equatable {
  final String name;

  final List<String> genre;
  final String plot;
  final String yearOfPublication;
  final String imageUrl;
  final List<EpisodeLink> episodes;

  AnimePage({
    @required this.name,
    @required this.genre,
    @required this.plot,
    @required this.yearOfPublication,
    @required this.imageUrl,
    @required this.episodes,
  });

  @override
  List<Object> get props => [
        name,
        genre,
        plot,
        yearOfPublication,
        imageUrl,
        episodes,
      ];
}

class EpisodeLink extends Equatable {
  final String identifier;

  /// points to html streaming page
  final Uri uri;

  final bool downloadable;

  EpisodeLink({
    this.identifier,
    this.uri,
    this.downloadable = true,
  });

  @override
  List<Object> get props => [identifier, uri];
}

class StreamingPage extends Equatable {
  final Uri source;

  StreamingPage(this.source);

  @override
  List<Object> get props => [source];
}
