
abstract class AnimeForceWebsite {
  Future<String> fetchAnimeListPage();
  Future<String> fetchAnime(Uri url);
  Future<String> fetchStreamingPage(Uri url);
}