import 'package:anime_force/src/core/port/access.dart';
import 'package:anime_force/src/infrastructure/access/http.dart';
import 'package:anime_force/src/presentation/ui/core/component/anime_list/widget.dart';
import 'package:flutter/material.dart';

AnimeForceWebsite websiteInstance;

class MyApp extends StatelessWidget {
  MyApp(String url) {
    websiteInstance = HttpAnimeForceWebsite(url);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Anime Force',
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
      ),
      home: AnimeListPage(),
    );
  }
}
