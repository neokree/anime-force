import 'package:anime_force/src/core/component/anime/domain/website.dart';
import 'package:anime_force/src/core/port/access.dart';
import 'package:anime_force/src/core/port/parsing.dart';
import 'package:anime_force/src/presentation/ui/core/component/anime_list/view_model.dart';
import 'package:mobx/mobx.dart';

part 'observable.g.dart';

class HomeStore = _HomeStore with _$HomeStore;

abstract class _HomeStore with Store {
  final AnimeForceWebsite _animeForceWebsite;
  final Parser _parser;

  _HomeStore(this._animeForceWebsite, this._parser);

  @observable
  List<AnimeLink> animeList = [];

  @action
  fetchAnimeList() async {
    if (_animeForceWebsite == null) return;
    String webPage = await _animeForceWebsite.fetchAnimeListPage();
    animeList = _parser.parseAnimeListPage(webPage).animeList;

    animeList.sort((a, b) {
      return a.name.compareTo(b.name);
    });
  }

  // AnimeList

  @computed
  List<SectionVM> get animeListByLetter {
    List<String> headers = animeList.fold(<String>[], (headers, anime) {
      String initial = anime.name.substring(0, 1).toUpperCase();
      if (!headers.contains(initial)) {
        headers.add(initial);
      }
      return headers;
    });

    return headers.map((initial) {
      return SectionVM(
        initial,
        animeList
            .where((anime) => anime.name.startsWith(initial))
            .toList(growable: false),
      );
    }).toList(growable: false);
  }

  /// Query string used for search by name inside anime list
  @observable
  String query;

  @computed
  List<AnimeLink> get searchResults {
    return animeList
        .where(
            (anime) => anime.name.toLowerCase().contains(query.toLowerCase()))
        .toList();
  }
}
