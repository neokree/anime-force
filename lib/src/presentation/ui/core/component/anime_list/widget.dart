import 'package:anime_force/src/core/component/anime/domain/website.dart';
import 'package:anime_force/src/infrastructure/parsing/html.dart';
import 'package:anime_force/src/presentation/ui/core/component/anime_detail/widget.dart';
import 'package:anime_force/src/presentation/ui/core/component/anime_list/observable.dart';
import 'package:anime_force/src/presentation/ui/core/component/app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:sticky_headers/sticky_headers.dart';

class AnimeListPage extends StatelessWidget {
  final HomeStore _store = HomeStore(websiteInstance, HtmlParser());

  AnimeListPage() {
    _store.fetchAnimeList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Anime Force'),
        actions: <Widget>[
          IconButton(
            key: ValueKey('search button'),
            icon: Icon(Icons.search),
            onPressed: () => showSearch(
              context: context,
              delegate: AnimeSearchDelegate(_store, searchFieldLabel: 'Cerca'),
            ),
          )
        ],
      ),
      body: Observer(builder: (context) {
        return ListView.builder(
          key: ValueKey('Anime List'),
          itemCount: _store.animeListByLetter.length,
          itemBuilder: (context, index) {
            var section = _store.animeListByLetter[index];
            return StickyHeader(
              header: Container(
                color: Colors.green,
                padding:
                    const EdgeInsets.symmetric(horizontal: 16.0, vertical: 4.0),
                alignment: Alignment.centerLeft,
                child: Text(
                  section.header,
                  style: TextStyle(color: Colors.white),
                ),
              ),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: section.animeList.map(
                      (link) => ListTile(
                    key: ValueKey(link.name),
                    title: Text(link.name),
                    onTap: () => _openDetailPage(context, link),
                  ),
                ).toList(growable: false),
              ),
            );
          },
        );
      }),
    );
  }
}

class AnimeSearchDelegate extends SearchDelegate {
  final HomeStore _store;

  AnimeSearchDelegate(this._store, {String searchFieldLabel})
      : super(searchFieldLabel: searchFieldLabel);

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
          _store.query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return _makeListView(_store.searchResults);
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // This method is called everytime the search term changes.
    // If you want to add search suggestions as the user enters their search term, this is the place to do that.
    _store.query = query;

    return _makeListView(_store.searchResults);
  }

  /// Returns the list of anime that matched the search
  ListView _makeListView(List<AnimeLink> results) {
    return ListView.builder(
      key: ValueKey('Anime List'),
      itemCount: results.length,
      itemBuilder: (context, index) {
        var anime = results[index];
        return ListTile(
          key: ValueKey('anime-n$index'),
          title: Text(anime.name, key: ValueKey(anime.name)),
          onTap: () {
            close(context, null);
            _openDetailPage(context, anime);
          },
        );
      },
    );
  }
}

_openDetailPage(BuildContext context, AnimeLink anime) {
  Navigator.push(context,
      MaterialPageRoute(builder: (context) => AnimeDetailPage(anime.url)));
}
