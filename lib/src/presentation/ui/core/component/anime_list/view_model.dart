

import 'package:anime_force/src/core/component/anime/domain/website.dart';

class SectionVM {
  final String header;
  final List<AnimeLink> animeList;

  SectionVM(this.header, this.animeList);
}