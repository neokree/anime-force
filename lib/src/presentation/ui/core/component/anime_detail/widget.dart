import 'package:anime_force/src/core/component/anime/domain/website.dart';
import 'package:anime_force/src/infrastructure/parsing/html.dart';
import 'package:anime_force/src/presentation/ui/core/component/anime_detail/observable.dart';
import 'package:anime_force/src/presentation/ui/core/component/app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:url_launcher/url_launcher.dart';

class AnimeDetailPage extends StatelessWidget {
  final AnimeDetailStore _store =
      AnimeDetailStore(websiteInstance, HtmlParser());

  AnimeDetailPage(Uri url) {
    _store.loadAnime(url);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: ValueKey('Anime Detail'),
      body: Observer(
        builder: (context) {
          var anime = _store.anime;
          if (anime == null) return Container();

          return CustomScrollView(slivers: [
            SliverAppBar(
              expandedHeight: MediaQuery.of(context).size.height * 0.5,
              flexibleSpace: FlexibleSpaceBar(
                background: Stack(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        image: anime.imageUrl != null
                            ? DecorationImage(
                                fit: BoxFit.cover,
                                image: NetworkImage(
                                  anime.imageUrl,
                                ),
                              )
                            : null,
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: FractionalOffset.topCenter,
                          end: FractionalOffset.bottomCenter,
                          colors: [
                            Colors.black.withOpacity(0.2),
                            Colors.black.withOpacity(0.8),
                          ],
                          stops: [0.0, 1.0],
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 16.0, right: 16.0, bottom: 8),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: Card(
                                child: AspectRatio(
                                  aspectRatio: 27 / 40,
                                  child: anime.imageUrl != null
                                      ? Image.network(
                                          anime.imageUrl,
                                          fit: BoxFit.contain,
                                        )
                                      : null,
                                ),
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                              ),
                            ),
                            Container(width: 16),
                            Expanded(
                              flex: 2,
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    anime.name ?? '',
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline
                                        .copyWith(color: Colors.white),
                                    key: ValueKey('name'),
                                  ),
                                  Text(anime.genre?.join(', ') ?? '',
                                      style: Theme.of(context)
                                          .textTheme
                                          .caption
                                          .copyWith(color: Colors.white)),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            SliverPadding(
              padding: const EdgeInsets.all(16.0),
              sliver: SliverList(
                delegate: SliverChildListDelegate(
                  [
                    Text(anime.plot ?? ''),
                    Container(height: 16),
                    Row(
                      children: <Widget>[
                        Text('Anno:',
                            style: Theme.of(context).textTheme.subtitle),
                        Container(width: 8.0),
                        Expanded(child: Text(anime.yearOfPublication ?? '')),
                      ],
                    ),
                    Container(height: 16),
                    Column(
                      children: anime.episodes
                          .map((link) => ListTile(
                                key: ValueKey(link.identifier),
                                title: Text(link.identifier),
                                leading: Icon(
                                  link.downloadable
                                      ? Icons.check_circle
                                      : Icons.block,
                                  color: link.downloadable
                                      ? Colors.green
                                      : Colors.red,
                                ),
                                onTap: () => _openEpisode(link, context),
                              ))
                          .toList(),
                    ),
                  ],
                ),
              ),
            ),
          ]);
        },
      ),
    );
  }

  Future _openEpisode(EpisodeLink link, BuildContext context) async {
    /// find the url of the episode.
    /// try to use the direct one, otherwise go for the streaming page

    if (_store.episodes[link] != null) {
      String url = _store.episodes[link].source.toString();
      if (await canLaunch(url)) {
        print('opening url: $url');
        await launch(url);
        return;
      }
    }

    String url = link.uri.toString();
    if (await canLaunch(url)) {
      print('opening url: $url');
      await launch(url);
      return;
    }

    await showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          key: ValueKey('opening error'),
          title: Text('Errore di apertura'),
          actions: <Widget>[
            FlatButton(
              onPressed: () => Navigator.pop(context),
              child: Text('OK'),
            )
          ],
        );
      },
    );
  }
}
