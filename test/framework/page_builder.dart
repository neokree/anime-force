import 'dart:math';

import 'package:anime_force/src/core/component/anime/domain/website.dart';

class PageBuilder {
  final Uri baseUri;

  PageBuilder(this.baseUri);

  String buildAnimeListPage(List<AnimeLink> animeList) {
    String content = _formatAnimeListContent(animeList);
    return _buildAnimeListPage(content);
  }

  String buildAnimeDetailPage(AnimePage anime) {
    String content = _formatAnimeContent(anime);
    return _buildAnimeDetailPage(
        anime.name + ' ' + _randomAnimeSuffix(), content);
  }

  String buildEpisodeVideoPage(StreamingPage episode) {
    return _buildEpisodeVideoPage(episode.source.toString());
  }

  String _formatAnimeListContent(List<AnimeLink> animeList) {
    return '<div class="item-wrapper">'
            '<div class="bg-white rounded shadow border anime-item">'
            '<div class=" media text-muted pt-3">'
            '<div class="media-body pb-3 mb-0 small">'
            '<div class="">' +
        animeList
            .map((link) =>
                '<a href="${baseUri.resolveUri(link.url).toString()}"><strong class="text-gray-dark">${link.name}</strong></a>')
            .join() +
        '</div>'
            '</div>'
            '</div>'
            '</div>'
            '</div>';
  }

  String _formatAnimeContent(AnimePage anime) {
    String episodes = anime.episodes.map((episode) {
      String redirectUrl = baseUri.resolveUri(episode.uri).toString();
      return ''' 
        <tr>
          <td style="text-align: left;"><strong>${episode.identifier}</strong></td>
          <td style="text-align: center;"><a href="$redirectUrl" target="_blank"><img src="/DDL/download.png" alt="" width="35" height="35" /></a></td>
          <td style="text-align: center;"><a href="$redirectUrl" target="_blank"><img src="/DDL/streaming.png" alt="" width="35" height="35" /></a></td>
          </tr>
        ''';
    }).join('');

    return '''
    <p style="text-align: center;"><img class="aligncenter wp-image-14988" src="${anime.imageUrl.replaceAll('http:', '')}" alt="" width="350" height="500" /></p>
    <table>
      <tbody>
      <tr>
      <td width="10%"><strong>Fansub</strong></td>
      <td><a href="//akasubs.altervista.org/">AkaSubs</a></td>
      </tr>
      <tr>
      <td width="10%"><strong>Titolo</strong></td>
      <td>${anime.name} ${_randomAnimeSuffix()}</td>
      </tr>
      <tr>
      <td width="10%"><strong>Genere</strong></td>
      <td>${anime.genre.join(', ')}</td>
      </tr>
      <tr>
      <td><strong>Trama</strong></td>
      <td>${anime.plot}</td>
      </tr>
      <tr>
      <td><strong>Anno</strong></td>
      <td>${anime.yearOfPublication}</td>
      </tr>
      <tr>
      <td><strong>Episodi</strong></td>
      <td>${anime.episodes.length}</td>
      </tr>
      </tbody>
    </table>
<table style="width: 100%;">
<tbody>
<tr>
<th style="text-align: center;" width="5%">
<h6>Episodio</h6>
</th>
<th style="text-align: center;" width="10%">
<h6>Download</h6>
</th>
<th style="text-align: center;" width="10%">
<h6>Streaming</h6>
</th>
</tr>
$episodes
</tbody>
</table>
    ''';
  }

  String _randomAnimeSuffix() {
    var suffixes = ['Sub Ita Download & Streaming', 'Sub Ita Streaming'];
    return suffixes[Random().nextInt(2)];
  }

  String _buildAnimeListPage(String content) => '''
<!doctype html>
<html lang="it-IT" xmlns:fb="http://ogp.me/ns/fb#" prefix="og: http://ogp.me/ns#">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="https://gmpg.org/xfn/11">
<title>Lista Anime A-Z - AnimeForce</title>

<link rel="canonical" href="https://ww1.animeforce.org/lista-anime/" />
<meta property="og:locale" content="it_IT" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Lista Anime A-Z - AnimeForce" />
<meta property="og:url" content="https://ww1.animeforce.org/lista-anime/" />
<meta property="og:site_name" content="AnimeForce" />
<meta property="article:publisher" content="https://www.facebook.com/animeforce/" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="Lista Anime A-Z - AnimeForce" />

<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="AnimeForce &raquo; Feed" href="https://ww1.animeforce.org/feed/" />
<link rel="alternate" type="application/rss+xml" title="AnimeForce &raquo; Feed dei commenti" href="https://ww1.animeforce.org/comments/feed/" />
<link rel="alternate" type="application/rss+xml" title="AnimeForce &raquo; Lista Anime A-Z Feed dei commenti" href="https://ww1.animeforce.org/lista-anime/feed/" />
<script type="text/javascript">
window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.2.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/ww1.animeforce.org\/wp-includes\/js\/wp-emoji-release.min.js"}};
!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),!(j.toDataURL().length<3e3)&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,65039,8205,55356,57096),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57331,55356,57096),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55357,56425,55356,57341,8205,55357,56507),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55357,56425,55356,57341,55357,56507),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
display: inline !important;
border: none !important;
box-shadow: none !important;
height: 1em !important;
width: 1em !important;
margin: 0 .07em !important;
vertical-align: -0.1em !important;
background: none !important;
padding: 0 !important;
}
</style>
















<script src='//ww1.animeforce.org/wp-content/cache/wpfc-minified/9aqlg857/dy4vu.js' type="text/javascript"></script>









<link rel='https://api.w.org/' href='https://ww1.animeforce.org/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://ww1.animeforce.org/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://ww1.animeforce.org/wp-includes/wlwmanifest.xml" />
<meta name="generator" content="WordPress 4.7.6" />
<link rel='shortlink' href='https://ww1.animeforce.org/?p=14557' />
<link rel="alternate" type="application/json+oembed" href="https://ww1.animeforce.org/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fww1.animeforce.org%2Flista-anime%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://ww1.animeforce.org/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fww1.animeforce.org%2Flista-anime%2F&#038;format=xml" />
<script type="text/javascript">
var adfly_id = "16031519";
var adfly_advert = "int";
var adfly_domain = "j.gs";
var adfly_protocol = "http";
var domains = ["animeforce.org\/ds", "animeforce.org\/ds016", "bit.ly"]; 
</script>
<script src="https://cdn.adf.ly/js/link-converter.js"></script>

<link rel="stylesheet" type="text/css" href="//ww1.animeforce.org/wp-content/cache/wpfc-minified/l1z38ga4/e0igd.css" media="all" /><meta property="fb:app_id" content="375138642670750" /> <script type="text/javascript">
var ajaxurl = 'https://ww1.animeforce.org/wp-admin/admin-ajax.php';
</script>
<script type='text/javascript' src='//padsblue.com/83/76/de/8376de1dd13d9aa0a42c8725afb7847e.js'></script></head>
<body class="page-template page-template-azlist page-template-azlist-php page page-id-14557">
<div id="page" class="site">
<a class="skip-link screen-reader-text" href="#content">Salta al contenuto</a>
<nav class=" navbar navbar-expand-lg  navbar-dark bg-dark ">
<a class="navbar-brand" href="https://ww1.animeforce.org">AnimeForce</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarSupportedContent">









<nav id="navbarSupportedContent" class="collapse navbar-collapse main-menu"><ul id="menu-mediaforce" class="navbar-nav mr-auto"><li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-90212" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-14557 current_page_item active menu-item-90212 nav-item"><a title="Lista Anime A-Z" href="https://ww1.animeforce.org/lista-anime/" class="nav-link">Lista Anime A-Z</a></li>
<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-90213" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-90213 nav-item"><a title="Anime in Corso" href="https://ww1.animeforce.org/category/anime/anime-in-corso/" class="nav-link">Anime in Corso</a></li>
<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-90214" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-90214 nav-item"><a title="Anime Uscite Irregolari" href="https://ww1.animeforce.org/category/anime/anime-con-uscite-irregolari/" class="nav-link">Anime Uscite Irregolari</a></li>
<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-90215" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-90215 nav-item"><a title="Contatti" href="https://ww1.animeforce.org/contacts/" class="nav-link">Contatti</a></li>
<form class="form-inline my-2 my-lg-0" role="search" method="get" id="searchform" action="https://ww1.animeforce.org/" class="form-inline my-2 my-lg-0">
<div><label class="screen-reader-text" for="s">Search for:</label>
<input class="form-control mr-sm-2" type="text" value="" name="s" id="s" />
<input class="btn my-2 my-sm-0" type="submit" id="searchsubmit" value="Cerca" />
</div>
</form></ul></nav>








</div>
</nav>
<div id="content" class="site-content">
<div class="container">
<h2 style="text-align: center">Per cercare un anime, premi CTRL + F</h2>
<div id="az-tabs" class="my-4">
<div id="letters">
<div class="az-letters">
<ul class="az-links"><li class="first odd has-posts"><a href="#letter-A"><span>A</span></a></li><li class="even has-posts"><a href="#letter-B"><span>B</span></a></li><li class="odd has-posts"><a href="#letter-C"><span>C</span></a></li><li class="even has-posts"><a href="#letter-D"><span>D</span></a></li><li class="odd has-posts"><a href="#letter-E"><span>E</span></a></li><li class="even has-posts"><a href="#letter-F"><span>F</span></a></li><li class="odd has-posts"><a href="#letter-G"><span>G</span></a></li><li class="even has-posts"><a href="#letter-H"><span>H</span></a></li><li class="odd has-posts"><a href="#letter-I"><span>I</span></a></li><li class="even has-posts"><a href="#letter-J"><span>J</span></a></li><li class="odd has-posts"><a href="#letter-K"><span>K</span></a></li><li class="even has-posts"><a href="#letter-L"><span>L</span></a></li><li class="odd has-posts"><a href="#letter-M"><span>M</span></a></li><li class="even has-posts"><a href="#letter-N"><span>N</span></a></li><li class="odd has-posts"><a href="#letter-O"><span>O</span></a></li><li class="even has-posts"><a href="#letter-P"><span>P</span></a></li><li class="odd has-posts"><a href="#letter-Q"><span>Q</span></a></li><li class="even has-posts"><a href="#letter-R"><span>R</span></a></li><li class="odd has-posts"><a href="#letter-S"><span>S</span></a></li><li class="even has-posts"><a href="#letter-T"><span>T</span></a></li><li class="odd has-posts"><a href="#letter-U"><span>U</span></a></li><li class="even has-posts"><a href="#letter-V"><span>V</span></a></li><li class="odd has-posts"><a href="#letter-W"><span>W</span></a></li><li class="even has-posts"><a href="#letter-X"><span>X</span></a></li><li class="odd has-posts"><a href="#letter-Y"><span>Y</span></a></li><li class="even has-posts"><a href="#letter-Z"><span>Z</span></a></li><li class="last odd has-posts"><a href="#letter-_"><span>#</span></a></li></ul> </div>
</div>
<div id="az-slider">
<div id="inner-slider">
<div class="letter-section" id="letter-A">
<h2 class="letter-title">
<span>
A </span>
</h2>
<div class="list-item">

${content}

</div>
<div class="back-to-top">
<a href="#letters">
Torna su </a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<footer class="footer mt-auto py-3">
<div class="container">
<div class="row">
<div class="col-6"><span class="text-muted">AnimeForce 2019 &copy;</span></div>
<div class="col-6"><span class="text-muted">Copyright © 2019 AnimeForce, All Rights Reserved.<br>
All files on this site are the property of their respective and rightful owners.
Info/Abuse: <a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="fc9d929591999a938e9f99d2938e9bbc9b919d9590d29f9391">[email&#160;protected]</a></span></div>
</div>
</div>
</footer>
</div>
<style type="text/css">#cookieChoiceInfo{background-color: #fff;color: #000;left:0;margin:0;padding:4px;position:fixed;text-align:center;top:0;width:100%;z-index:9999;}.italybtn{margin-left:10px;}</style><script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script>var coNA="displayCookieConsent",coVA="y";scroll="",elPos="fixed",infoClass="italybtn",closeClass="italybtn",htmlM="",rel="",tar="",bgB="#fff",btcB="#000",bPos="top:0",bannerStyle="bannerStyle",contentStyle="contentStyle",consText="consentText",jsArr = [];function allowCookie(){var a,b=document.getElementsByClassName("el"),c=new RegExp("<script.*?");for(a=0;a<b.length;a++){b[a].removeChild(b[a].childNodes[0]);var d=c.test(jsArr[a]);if(d){var e=/<script.*?src="(.*?)"/,f=e.test(jsArr[a]);f&&(f=e.exec(jsArr[a]),loadJS(f[1]));var g=/<script\b[^>]*>([\s\S]*?)<\/script>/gm,h=g.exec(jsArr[a]);h[1]&&appendJS(h[1])}else{var i=b[a].innerHTML;d=i.replace(/<cookie>/g,jsArr[a]),b[a].innerHTML=d}}}function loadJS(a){var b=document.createElement("script");b.type="application/javascript",b.src=a,document.body.appendChild(b)}function appendJS(a){var b=document.createElement("script");b.type="text/javascript";var c=a;try{b.appendChild(document.createTextNode(c)),document.body.appendChild(b)}catch(d){b.text=c,document.body.appendChild(b)}}!function(a){if(a.cookieChoices)return a.cookieChoices;var b=a.document,c=(b.documentElement,"textContent"in b.body,function(){function a(a){var b=a.offsetHeight,c=getComputedStyle(a);return b+=parseInt(c.marginTop)+parseInt(c.marginBottom)}function c(a,c,d,e){var i=b.createElement("div");i.id=r,i.className=bannerStyle;var j=b.createElement("div");return j.className=contentStyle,j.appendChild(f(a)),d&&e&&j.appendChild(h(d,e)),j.appendChild(g(c)),i.appendChild(j),i}function d(a,c,d,e){var i=b.createElement("div");i.id=r;var j=b.createElement("div");j.className="glassStyle";var k=b.createElement("div");k.className=contentStyle;var l=b.createElement("div");l.className=bannerStyle;var m=g(c);return k.appendChild(f(a)),d&&e&&k.appendChild(h(d,e)),k.appendChild(m),l.appendChild(k),i.appendChild(j),i.appendChild(l),i}function e(a,b){a.innerHTML=b}function f(a){var c=b.createElement("span");return c.className=consText,e(c,a),c}function g(a){var c=b.createElement("a");return e(c,a),c.id=s,c.className=closeClass,c.href="#",c}function h(a,c){var d=b.createElement("a");return e(d,a),d.className=infoClass,d.href=c,tar&&(d.target="_blank"),d}function i(){return p()&&(htmlM&&(b.getElementsByTagName("html")[0].style.marginTop=t),allowCookie(),o(),m()),rel&&b.location.reload(),!1}function j(e,f,g,h,j){if(p()){var k=j?d(e,f,g,h):c(e,f,g,h),l=b.createDocumentFragment();l.appendChild(k),b.body.appendChild(l.cloneNode(!0)),htmlM&&(b.getElementsByTagName("html")[0].style.marginTop=a(b.getElementById("cookieChoiceInfo"))+"px"),b.getElementById(s).onclick=i,scroll&&(b.onscroll=i)}}function k(a,b,c,d){j(a,b,c,d,!1)}function l(a,b,c,d){j(a,b,c,d,!0)}function m(){var a=b.getElementById(r);null!==a&&a.parentNode.removeChild(a)}function n(){i()}function o(){var a=new Date;a.setFullYear(a.getFullYear()+1),b.cookie=q+"="+coVA+"; expires="+a.toGMTString()+";path=/"}function p(){return!b.cookie.match(new RegExp(q+"=([^;]+)"))}var q=coNA,r="cookieChoiceInfo",s="cookieChoiceDismiss",t=b.getElementsByTagName("html")[0].style.marginTop,u={};return u.showCookieConsentBar=k,u.showCookieConsentDialog=l,u.removeCookieConsent=n,u}());return a.cookieChoices=c,c}(this);document.addEventListener("DOMContentLoaded", function(event) {cookieChoices.showCookieConsentBar("La legge ci obbliga a dirvi che il sito utilizza cookies di terze parti. Continuando con la navigazione accetti le nostre modalit\u00e0 d'uso dei cookie.", "Accetto", "Maggiori Info", "//www.allaboutcookies.org/");});</script><noscript><style type="text/css">html{margin-top:35px}</style><div id="cookieChoiceInfo"><span>"La legge ci obbliga a dirvi che il sito utilizza cookies di terze parti. Continuando con la navigazione accetti le nostre modalit\u00e0 d'uso dei cookie."</span><a href="//www.allaboutcookies.org/" class="italybtn" target="_blank">Maggiori Info</a></div></noscript> <script>
jQuery( document ).ready( function() {
jQuery( '.mediablender-lightbox-link' ).mediablender({
type: 'lightbox',
security: 'a73fa0f428',
});
jQuery( '.mediablender-slider' ).mediablender({
type: 'slider',
security: 'a73fa0f428',
});
jQuery( '.mediablender-list-item' ).mediablender({
type: 'list',
security: 'a73fa0f428',
});
});
</script>
<script>
jQuery( document ).ready( function() {
jQuery('video,audio').mediaelementplayer({
videoVolume: 'horizontal',
success: function(player, node) {
//jQuery(node).parents('.mejs-container').css('position','relative');
//jQuery(node).parents('.mejs-container').append('<p style="position:absolute;z-index:999999;top:20px;left:20px;color:#FFF;background:#000;">Succes (mode: ' + player.pluginType+')</p>');
}
});
});
</script>
<script type='text/javascript'>
/* <![CDATA[ */
var btf_localization = {"ajaxurl":"https:\/\/ww1.animeforce.org\/wp-admin\/admin-ajax.php","min_search":"8","allow_clear":"","show_description":"","disable_select2":"1","conditional_dropdowns":"","language":"","rtl":"","disable_fuzzy":"","show_count":""};
/* ]]> */
</script>
<script type='text/javascript' src='https://ww1.animeforce.org/wp-content/plugins/beautiful-taxonomy-filters/public/js/beautiful-taxonomy-filters-public.js'></script>
<script type='text/javascript' src='https://ww1.animeforce.org/wp-content/plugins/mediablender/js/jquery.cookie.js'></script>
<script type='text/javascript' src='https://ww1.animeforce.org/wp-content/plugins/mediablender/js/jquery.easing.js'></script>
<script type='text/javascript' src='https://ww1.animeforce.org/wp-content/plugins/mediablender/js/jquery.timers-1.1.3.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var mediablender_vars = {"cookie_hash":"411324dbd3f7db9a34c06e513e78023b","comment_text":"Comments","comment_single":"Comment","nav_settings":{"nav_before":"","nav_after":"","nav_link_before":"","nav_link_after":""}};
/* ]]> */
</script>
<script type='text/javascript' src='https://ww1.animeforce.org/wp-content/plugins/mediablender/js/mediablender.js'></script>
<script type='text/javascript' src='https://ww1.animeforce.org/wp-content/plugins/mediablender/metatools/libs/mediaelement/mediaelement-and-player.js'></script>
<script type='text/javascript' src='https://ww1.animeforce.org/wp-content/plugins/mediablender/metatools/js/metatools.js'></script>
<script type='text/javascript' src='https://ww1.animeforce.org/wp-content/themes/mediaforce/js/mediaforce.js'></script>
<script type='text/javascript' src='https://ww1.animeforce.org/wp-content/themes/mediaforce/js/navigation.js'></script>
<script type='text/javascript' src='https://ww1.animeforce.org/wp-content/themes/mediaforce/js/skip-link-focus-fix.js'></script>
<script type='text/javascript' src='https://ww1.animeforce.org/wp-includes/js/comment-reply.min.js'></script>
<script type='text/javascript' src='https://ww1.animeforce.org/wp-includes/js/wp-embed.min.js'></script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = "//connect.facebook.net/it_IT/sdk.js#xfbml=1&appId=375138642670750&version=v2.3";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script type="text/javascript">var _Hasync= _Hasync|| [];
_Hasync.push(['Histats.start', '1,2004294,4,0,0,0,00010000']);
_Hasync.push(['Histats.fasi', '1']);
_Hasync.push(['Histats.track_hits', '']);
(function() {
var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
hs.src = ('//s10.histats.com/js15_as.js');
(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
})();</script>
<noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?2004294&101" alt="statistiche per siti web" border="0"></a></noscript>


<script async src="https://www.googletagmanager.com/gtag/js?id=UA-68140415-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-68140415-1');
</script>
</body>
</html>
  ''';

  String _buildAnimeDetailPage(String title, String content) => '''
  <!doctype html>
<html lang="it-IT" xmlns:fb="http://ogp.me/ns/fb#" prefix="og: http://ogp.me/ns#">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="https://gmpg.org/xfn/11">
<link rel="shortcut icon" href="https://ww1.animeforce.org/wp-content/uploads/2019/10/favicon.png" /><link>
<title>Ace of Diamond 2 Sub Ita Download &amp; Streaming - AnimeForce</title>

<link rel="canonical" href="https://ww1.animeforce.org/ace-of-diamond-2-sub-ita-download-streaming/" />
<meta property="og:locale" content="it_IT" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Ace of Diamond 2 Sub Ita Download &amp; Streaming - AnimeForce" />
<meta property="og:description" content="Fansub AkaSubs Titolo Ace of Diamond 2 Sub Ita Download &amp; Streaming Genere Commedia, Sport, Scolastico, Shounen Trama Seconda stagione di &#8220;Ace of Diamond&#8220;. Il diamante è quello disegnato dalle basi di gioco sul campo di baseball; l&#8217;asso in questione, il quindicenne Eijun Sawamura, lanciatore originario di Nagano che lascia il suo paese per frequentare &hellip;" />
<meta property="og:url" content="https://ww1.animeforce.org/ace-of-diamond-2-sub-ita-download-streaming/" />
<meta property="og:site_name" content="AnimeForce" />
<meta property="article:publisher" content="https://www.facebook.com/animeforce/" />
<meta property="article:tag" content="Ace of Diamond" />
<meta property="article:tag" content="Ace of Diamond 2" />
<meta property="article:tag" content="Spring 2015" />
<meta property="article:section" content="ANIME" />
<meta property="article:published_time" content="2015-02-21T14:47:52+00:00" />
<meta property="article:modified_time" content="2018-10-20T16:43:17+00:00" />
<meta property="og:updated_time" content="2018-10-20T16:43:17+00:00" />
<meta property="og:image" content="https://ww1.animeforce.org/wp-content/uploads/2015/02/AceSecond.jpg" />
<meta property="og:image:width" content="640" />
<meta property="og:image:height" content="960" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:description" content="Fansub AkaSubs Titolo Ace of Diamond 2 Sub Ita Download &amp; Streaming Genere Commedia, Sport, Scolastico, Shounen Trama Seconda stagione di &#8220;Ace of Diamond&#8220;. Il diamante è quello disegnato dalle basi di gioco sul campo di baseball; l&#8217;asso in questione, il quindicenne Eijun Sawamura, lanciatore originario di Nagano che lascia il suo paese per frequentare [&hellip;]" />
<meta name="twitter:title" content="Ace of Diamond 2 Sub Ita Download &amp; Streaming - AnimeForce" />
<meta name="twitter:image" content="https://ww1.animeforce.org/wp-content/uploads/2015/02/AceSecond.jpg" />

<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="AnimeForce &raquo; Feed" href="https://ww1.animeforce.org/feed/" />
<link rel="alternate" type="application/rss+xml" title="AnimeForce &raquo; Feed dei commenti" href="https://ww1.animeforce.org/comments/feed/" />
<link rel="alternate" type="application/rss+xml" title="AnimeForce &raquo; Ace of Diamond 2 Sub Ita Download &amp; Streaming Feed dei commenti" href="https://ww1.animeforce.org/ace-of-diamond-2-sub-ita-download-streaming/feed/" />








<link rel='https://api.w.org/' href='https://ww1.animeforce.org/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://ww1.animeforce.org/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://ww1.animeforce.org/wp-includes/wlwmanifest.xml" />
<meta name="generator" content="WordPress 4.7.6" />
<link rel='shortlink' href='https://ww1.animeforce.org/?p=30180' />
<link rel="alternate" type="application/json+oembed" href="https://ww1.animeforce.org/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fww1.animeforce.org%2Face-of-diamond-2-sub-ita-download-streaming%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://ww1.animeforce.org/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fww1.animeforce.org%2Face-of-diamond-2-sub-ita-download-streaming%2F&#038;format=xml" />
<script type="dbc7841092c2e942482490eb-text/javascript">
var adfly_id = "16031519";
var adfly_advert = "int";
var adfly_domain = "j.gs";
var adfly_protocol = "http";
var domains = ["animeforce.org\/ds", "animeforce.org\/ds016", "bit.ly"]; 
</script>
<script src="https://cdn.adf.ly/js/link-converter.js" type="dbc7841092c2e942482490eb-text/javascript"></script>

<link rel="stylesheet" type="text/css" href="//ww1.animeforce.org/wp-content/cache/wpfc-minified/dg4c72hc/hmyql.css" media="all" /><meta property="fb:app_id" content="375138642670750" /> <script type="dbc7841092c2e942482490eb-text/javascript" src='//padsblue.com/83/76/de/8376de1dd13d9aa0a42c8725afb7847e.js'></script></head>
<body class="post-template-default single single-post postid-30180 single-format-standard btf-archive">
<div id="page" class="site">
<a class="skip-link screen-reader-text" href="#content">Salta al contenuto</a>
<nav class=" navbar navbar-expand-lg  navbar-dark bg-dark ">
<a class="navbar-brand" href="https://ww1.animeforce.org">
<img class="site-logo" src="https://ww1.animeforce.org/wp-content/uploads/2019/10/logo.png" alt="AnimeForce">
</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarSupportedContent">









<nav id="navbarSupportedContent" class="collapse navbar-collapse main-menu"><ul id="menu-mediaforce" class="navbar-nav mr-auto"><li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-90212" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-90212 nav-item"><a title="Lista Anime A-Z" href="https://ww1.animeforce.org/lista-anime/" class="nav-link">Lista Anime A-Z</a></li>
<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-90213" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-90213 nav-item"><a title="Anime in Corso" href="https://ww1.animeforce.org/category/anime/anime-in-corso/" class="nav-link">Anime in Corso</a></li>
<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-90214" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-90214 nav-item"><a title="Anime Uscite Irregolari" href="https://ww1.animeforce.org/category/anime/anime-con-uscite-irregolari/" class="nav-link">Anime Uscite Irregolari</a></li>
<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-90215" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-90215 nav-item"><a title="Contatti" href="https://ww1.animeforce.org/contacts/" class="nav-link">Contatti</a></li>
<form class="form-inline my-2 my-lg-0" role="search" method="get" id="searchform" action="https://ww1.animeforce.org/" class="form-inline my-2 my-lg-0">
<div><label class="screen-reader-text" for="s">Search for:</label>
<input class="form-control mr-sm-2" type="text" value="" name="s" id="s" />
<input type="hidden" name="cat" id="cat" value="6010" />
<input class="btn my-2 my-sm-0" type="submit" id="searchsubmit" value="Cerca" />
</div>
</form></ul></nav>








</div>
</nav>
<div id="content" class="site-content">
<div class="container">
<main id="main" class="site-main">
<div class="row mt-4">
<div id="post-30180" class="col-md-8 post-30180 post type-post status-publish format-standard has-post-thumbnail hentry category-anime category-articoli-principali category-index tag-ace-of-diamond tag-ace-of-diamond-2 tag-spring-2015">
<div class="entry-header">
<h1 class="entry-title">$title</h1> </div>
<div class="border shadow mt-3 colored-bg">
<div><script data-cfasync="false" type="text/javascript" src="//ashaidbit.club/tOkBAwzQTf1uWZB/14950"></script></div>
<div><script data-cfasync="false" type="text/javascript" src="//ashaidbit.club/tuPwlDGyeF3IXVu0y/14951"></script></div> </div>
<div class="entry-content">

$content

</div>

<div class="border shadow mt-3 mb-3 colored-bg">

<div id="M109623ScriptRootC739360">
<div id="M109623PreloadC739360">
</div>
<script type="dbc7841092c2e942482490eb-text/javascript">
(function(){
var D=new Date(),d=document,b='body',ce='createElement',ac='appendChild',st='style',ds='display',n='none',gi='getElementById',lp=d.location.protocol,wp=lp.indexOf('http')==0?lp:'https:';
var i=d[ce]('iframe');i[st][ds]=n;d[gi]("M109623ScriptRootC739360")[ac](i);try{var iw=i.contentWindow.document;iw.open();iw.writeln("<ht"+"ml><bo"+"dy></bo"+"dy></ht"+"ml>");iw.close();var c=iw[b];}
catch(e){var iw=d;var c=d[gi]("M109623ScriptRootC739360");}var dv=iw[ce]('div');dv.id="MG_ID";dv[st][ds]=n;dv.innerHTML=739360;c[ac](dv);
var s=iw[ce]('script');s.async='async';s.defer='defer';s.charset='utf-8';s.src=wp+"//jsc.mgid.com/a/n/animeforce.org.739360.js?t="+D.getUTCFullYear()+D.getUTCMonth()+D.getUTCDate()+D.getUTCHours();c[ac](s);})();
</script>
</div>
 </div>
<div class="entry-footer">
<span class="tags-links">Tags <a href="https://ww1.animeforce.org/tag/ace-of-diamond/" rel="tag">Ace of Diamond</a>, <a href="https://ww1.animeforce.org/tag/ace-of-diamond-2/" rel="tag">Ace of Diamond 2</a>, <a href="https://ww1.animeforce.org/tag/spring-2015/" rel="tag">Spring 2015</a></span> </div>
<div class="related">
<div class="related-anime-section bg-light border my-3 shadow">
<nav>
<div class="nav nav-tabs" id="nav-tab" role="tablist">
<span class="nav-item nav-link disabled">Anime Consigliati</span>
<a class="nav-item nav-link  active " id="nav-suggested-tab" data-toggle="tab" href="#nav-tab-season" role="tab" aria-controls="nav-suggested" aria-selected="true">Stagione</a>
<a class="nav-item nav-link " id="nav-suggested-tab" data-toggle="tab" href="#nav-tab-tags" role="tab" aria-controls="nav-suggested" aria-selected="true">Tag</a>
</div>
</nav>
<div class="tab-content" id="nav-tabContent">
<div class="tab-pane fade show  active " id="nav-tab-season" role="tabpanel" aria-labelledby="nav-suggested-tab">
<div class="m-2 row">
<div class="anime-card col-12 col-sm-6 col-md-4 col-lg-3">
<div class="card mb-4 shadow-sm">
<a href="https://ww1.animeforce.org/yamada-kun-to-7-nin-no-majo-sub-ita-download-streaming/"><img src="https://ww1.animeforce.org/wp-content/uploads/2015/02/Yamada-150x150.jpg" alt="Yamada-kun to 7-nin no Majo   "></a>
<div class="anime-card-overlay">
<p class="card-text text-center">
<a href="https://ww1.animeforce.org/yamada-kun-to-7-nin-no-majo-sub-ita-download-streaming/"> <span>Yamada-kun to 7-nin no Majo </span></a>
</p>
</div>
</div>
</div><div class="anime-card col-12 col-sm-6 col-md-4 col-lg-3">
<div class="card mb-4 shadow-sm">
<a href="https://ww1.animeforce.org/yahari-ore-no-seishun-love-come-wa-machigatteiru-zoku-sub-ita-download-streaming/"><img src="https://ww1.animeforce.org/wp-content/uploads/2015/02/Yahari2-150x150.jpg" alt="Yahari Ore no Seishun Love Come wa Machigatteiru. Zoku  "></a>
<div class="anime-card-overlay">
<p class="card-text text-center">
<a href="https://ww1.animeforce.org/yahari-ore-no-seishun-love-come-wa-machigatteiru-zoku-sub-ita-download-streaming/"> <span>Yahari Ore no Seishun Love Come wa Machigatteiru. Zoku </span></a>
</p>
</div>
</div>
</div><div class="anime-card col-12 col-sm-6 col-md-4 col-lg-3">
<div class="card mb-4 shadow-sm">
<a href="https://ww1.animeforce.org/vampire-holmes-sub-ita-download-streaming/"><img src="https://ww1.animeforce.org/wp-content/uploads/2015/03/VampireHolmes-150x150.jpg" alt="Vampire Holmes  "></a>
<div class="anime-card-overlay">
<p class="card-text text-center">
<a href="https://ww1.animeforce.org/vampire-holmes-sub-ita-download-streaming/"> <span>Vampire Holmes </span></a>
</p>
</div>
</div>
</div><div class="anime-card col-12 col-sm-6 col-md-4 col-lg-3">
<div class="card mb-4 shadow-sm">
<a href="https://ww1.animeforce.org/uta-no-prince-sama-maji-love-revolutions-sub-ita-download-streaming/"><img src="https://ww1.animeforce.org/wp-content/uploads/2015/03/UtaNoPrinceSama3-150x150.png" alt="Uta no Prince-sama: Maji Love Revolutions  "></a>
<div class="anime-card-overlay">
<p class="card-text text-center">
<a href="https://ww1.animeforce.org/uta-no-prince-sama-maji-love-revolutions-sub-ita-download-streaming/"> <span>Uta no Prince-sama: Maji Love Revolutions </span></a>
</p>
</div>
</div>
</div>
</div>
</div>
<div class="tab-pane fade show " id="nav-tab-tags" role="tabpanel" aria-labelledby="nav-suggested-tab">
<div class="m-2 row">
<div class="anime-card col-12 col-sm-6 col-md-4 col-lg-3">
<div class="card mb-4 shadow-sm">
<a href="https://ww1.animeforce.org/ace-of-diamond-act-ii-sub-ita-download-streaming/"><img src="https://ww1.animeforce.org/wp-content/uploads/2019/03/Diamond-no-Ace-Act-II-300x450.png" alt="Ace of Diamond: Act II  "></a>
<div class="anime-card-overlay">
<p class="card-text text-center">
<a href="https://ww1.animeforce.org/ace-of-diamond-act-ii-sub-ita-download-streaming/"> <span>Ace of Diamond: Act II </span></a>
</p>
</div>
</div>
</div><div class="anime-card col-12 col-sm-6 col-md-4 col-lg-3">
<div class="card mb-4 shadow-sm">
<a href="https://ww1.animeforce.org/ace-of-diamond-sub-ita-download-streaming/"><img src="https://ww1.animeforce.org/wp-content/uploads/2014/04/AceOfDiamond1-150x150.jpg" alt="Ace of Diamond   "></a>
<div class="anime-card-overlay">
<p class="card-text text-center">
<a href="https://ww1.animeforce.org/ace-of-diamond-sub-ita-download-streaming/"> <span>Ace of Diamond </span></a>
</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col-md-4 sidebar">
<div class="card text-center shadow ">
<section id="text-12" class="widget widget_text"> <div class="textwidget"><div class="fb-page my-2" data-href="https://www.facebook.com/animeforce/" data-tabs="" data-width="" data-height="" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/animeforce/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/animeforce/">AnimeForce</a></blockquote></div>
</div>
</section></div>
<div class="card text-center shadow my-3">
<div class="card-body">
<div id="rn_ad_native_y6gie"></div>
<script src="//cdn.run-syndicate.com/sdk/v1/n.js" type="dbc7841092c2e942482490eb-text/javascript"></script>
<script type="dbc7841092c2e942482490eb-text/javascript">
NativeAd({
element_id: "rn_ad_native_y6gie",
spot: "fdc5f551b28c495fa059ea8568da14d7",
type: "label-under",
cols: 1,
rows: 3,
mobileEnabled: false,
title: "",
titlePosition: "left",
adsByPosition: "right",
styles: {
"label": {
"font-family": "'Open Sans', sans-serif"
},
"headlineLink": {
"color": "#524d53",
"font-size": "14px",
"font-weight": "bold"
}
}
});
</script> </div>
</div> </div>
</div>
</main>
</div>
</div>
<div class="fixed-image1">
<img src="https://ww1.animeforce.org/wp-content/uploads/2019/10/trzcacak.rs-neko-girl-png-1560382.png" />
</div>
<footer class="footer mt-auto py-3">
<div class="container">
<div class="row">
<div class="col-12 col-md-6"><span class="text-muted">AnimeForce 2019 &copy;</span></div>
<div class="col-12 col-md-6"><span class="text-muted">Copyright © 2019 AnimeForce, All Rights Reserved.<br>
All files on this site are the property of their respective and rightful owners.
Info/Abuse: <a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="e2838c8b8f87848d908187cc8d9085a2858f838b8ecc818d8f">[email&#160;protected]</a></span></div>
</div>
</div>
</footer>
</div>
<style type="text/css">#cookieChoiceInfo{background-color: #fff;color: #000;left:0;margin:0;padding:4px;position:fixed;text-align:center;top:0;width:100%;z-index:9999;}.italybtn{margin-left:10px;}</style><script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script type="dbc7841092c2e942482490eb-text/javascript">var coNA="displayCookieConsent",coVA="y";scroll="",elPos="fixed",infoClass="italybtn",closeClass="italybtn",htmlM="",rel="",tar="",bgB="#fff",btcB="#000",bPos="top:0",bannerStyle="bannerStyle",contentStyle="contentStyle",consText="consentText",jsArr = [];function allowCookie(){var a,b=document.getElementsByClassName("el"),c=new RegExp("<script.*?");for(a=0;a<b.length;a++){b[a].removeChild(b[a].childNodes[0]);var d=c.test(jsArr[a]);if(d){var e=/<script.*?src="(.*?)"/,f=e.test(jsArr[a]);f&&(f=e.exec(jsArr[a]),loadJS(f[1]));var g=/<script\b[^>]*>([\s\S]*?)<\/script>/gm,h=g.exec(jsArr[a]);h[1]&&appendJS(h[1])}else{var i=b[a].innerHTML;d=i.replace(/<cookie>/g,jsArr[a]),b[a].innerHTML=d}}}function loadJS(a){var b=document.createElement("script");b.type="application/javascript",b.src=a,document.body.appendChild(b)}function appendJS(a){var b=document.createElement("script");b.type="text/javascript";var c=a;try{b.appendChild(document.createTextNode(c)),document.body.appendChild(b)}catch(d){b.text=c,document.body.appendChild(b)}}!function(a){if(a.cookieChoices)return a.cookieChoices;var b=a.document,c=(b.documentElement,"textContent"in b.body,function(){function a(a){var b=a.offsetHeight,c=getComputedStyle(a);return b+=parseInt(c.marginTop)+parseInt(c.marginBottom)}function c(a,c,d,e){var i=b.createElement("div");i.id=r,i.className=bannerStyle;var j=b.createElement("div");return j.className=contentStyle,j.appendChild(f(a)),d&&e&&j.appendChild(h(d,e)),j.appendChild(g(c)),i.appendChild(j),i}function d(a,c,d,e){var i=b.createElement("div");i.id=r;var j=b.createElement("div");j.className="glassStyle";var k=b.createElement("div");k.className=contentStyle;var l=b.createElement("div");l.className=bannerStyle;var m=g(c);return k.appendChild(f(a)),d&&e&&k.appendChild(h(d,e)),k.appendChild(m),l.appendChild(k),i.appendChild(j),i.appendChild(l),i}function e(a,b){a.innerHTML=b}function f(a){var c=b.createElement("span");return c.className=consText,e(c,a),c}function g(a){var c=b.createElement("a");return e(c,a),c.id=s,c.className=closeClass,c.href="#",c}function h(a,c){var d=b.createElement("a");return e(d,a),d.className=infoClass,d.href=c,tar&&(d.target="_blank"),d}function i(){return p()&&(htmlM&&(b.getElementsByTagName("html")[0].style.marginTop=t),allowCookie(),o(),m()),rel&&b.location.reload(),!1}function j(e,f,g,h,j){if(p()){var k=j?d(e,f,g,h):c(e,f,g,h),l=b.createDocumentFragment();l.appendChild(k),b.body.appendChild(l.cloneNode(!0)),htmlM&&(b.getElementsByTagName("html")[0].style.marginTop=a(b.getElementById("cookieChoiceInfo"))+"px"),b.getElementById(s).onclick=i,scroll&&(b.onscroll=i)}}function k(a,b,c,d){j(a,b,c,d,!1)}function l(a,b,c,d){j(a,b,c,d,!0)}function m(){var a=b.getElementById(r);null!==a&&a.parentNode.removeChild(a)}function n(){i()}function o(){var a=new Date;a.setFullYear(a.getFullYear()+1),b.cookie=q+"="+coVA+"; expires="+a.toGMTString()+";path=/"}function p(){return!b.cookie.match(new RegExp(q+"=([^;]+)"))}var q=coNA,r="cookieChoiceInfo",s="cookieChoiceDismiss",t=b.getElementsByTagName("html")[0].style.marginTop,u={};return u.showCookieConsentBar=k,u.showCookieConsentDialog=l,u.removeCookieConsent=n,u}());return a.cookieChoices=c,c}(this);document.addEventListener("DOMContentLoaded", function(event) {cookieChoices.showCookieConsentBar("La legge ci obbliga a dirvi che il sito utilizza cookies di terze parti. Continuando con la navigazione accetti le nostre modalit\u00e0 d'uso dei cookie.", "Accetto", "Maggiori Info", "//www.allaboutcookies.org/");});</script><noscript><style type="text/css">html{margin-top:35px}</style><div id="cookieChoiceInfo"><span>"La legge ci obbliga a dirvi che il sito utilizza cookies di terze parti. Continuando con la navigazione accetti le nostre modalit\u00e0 d'uso dei cookie."</span><a href="//www.allaboutcookies.org/" class="italybtn" target="_blank">Maggiori Info</a></div></noscript><script type="dbc7841092c2e942482490eb-text/javascript" src='https://ww1.animeforce.org/wp-includes/js/jquery/jquery.js'></script>
<script type="dbc7841092c2e942482490eb-text/javascript" src='https://ww1.animeforce.org/wp-includes/js/jquery/jquery-migrate.min.js'></script>
<script type="dbc7841092c2e942482490eb-text/javascript">
/* <![CDATA[ */
var btf_localization = {"ajaxurl":"https:\/\/ww1.animeforce.org\/wp-admin\/admin-ajax.php","min_search":"8","allow_clear":"","show_description":"","disable_select2":"1","conditional_dropdowns":"","language":"","rtl":"","disable_fuzzy":"","show_count":""};
/* ]]> */
</script>
<script type="dbc7841092c2e942482490eb-text/javascript" src='https://ww1.animeforce.org/wp-content/plugins/beautiful-taxonomy-filters/public/js/beautiful-taxonomy-filters-public.js'></script>
<script type="dbc7841092c2e942482490eb-text/javascript" src='https://ww1.animeforce.org/wp-content/themes/mediaforce/player/video-js-7.4.1/dist/video.min.js'></script>
<script type="dbc7841092c2e942482490eb-text/javascript" src='https://ww1.animeforce.org/wp-content/themes/mediaforce/player/videojs.persistvolume.js'></script>
<script type="dbc7841092c2e942482490eb-text/javascript" src='https://ww1.animeforce.org/wp-content/themes/mediaforce/player/videojs.hotkeys.min.js'></script>
<script type="dbc7841092c2e942482490eb-text/javascript" src='https://ww1.animeforce.org/wp-content/themes/mediaforce/jquery/jquery-3.3.1.min.js'></script>
<script type="dbc7841092c2e942482490eb-text/javascript" src='https://ww1.animeforce.org/wp-content/themes/mediaforce/js/mediaforce.js'></script>
<script type="dbc7841092c2e942482490eb-text/javascript" src='https://ww1.animeforce.org/wp-content/themes/mediaforce/bootstrap/js/bootstrap.min.js'></script>
<script type="dbc7841092c2e942482490eb-text/javascript" src='https://ww1.animeforce.org/wp-content/themes/mediaforce/js/navigation.js'></script>
<script type="dbc7841092c2e942482490eb-text/javascript" src='https://ww1.animeforce.org/wp-content/themes/mediaforce/js/skip-link-focus-fix.js'></script>
<script type="dbc7841092c2e942482490eb-text/javascript" src='https://ww1.animeforce.org/wp-includes/js/comment-reply.min.js'></script>
<script type="dbc7841092c2e942482490eb-text/javascript" src='https://ww1.animeforce.org/wp-includes/js/wp-embed.min.js'></script>
<script type="dbc7841092c2e942482490eb-text/javascript" src='https://ww1.animeforce.org/wp-content/plugins/easy-spoiler/js/easy-spoiler.js'></script>

<div id="fb-root"></div>
<script type="dbc7841092c2e942482490eb-text/javascript">(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s); js.id = id;
js.src = "//connect.facebook.net/it_IT/sdk.js#xfbml=1&appId=375138642670750&version=v2.3";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script type="dbc7841092c2e942482490eb-text/javascript">var _Hasync= _Hasync|| [];
_Hasync.push(['Histats.start', '1,2004294,4,0,0,0,00010000']);
_Hasync.push(['Histats.fasi', '1']);
_Hasync.push(['Histats.track_hits', '']);
(function() {
var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
hs.src = ('//s10.histats.com/js15_as.js');
(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
})();</script>
<noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?2004294&101" alt="statistiche per siti web" border="0"></a></noscript>


<script async src="https://www.googletagmanager.com/gtag/js?id=UA-68140415-1" type="dbc7841092c2e942482490eb-text/javascript"></script>
<script type="dbc7841092c2e942482490eb-text/javascript">
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-68140415-1');
</script>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/it_IT/sdk.js#xfbml=1&version=v4.0&appId=524339418399667" type="dbc7841092c2e942482490eb-text/javascript"></script>
<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/95c75768/cloudflare-static/rocket-loader.min.js" data-cf-settings="dbc7841092c2e942482490eb-|49" defer=""></script></body>
</html>
  ''';

  String _buildEpisodeVideoPage(String url) => '''
  <!DOCTYPE html>
<html>
<head>
<title>Download & Streaming </title>
<link rel="shortcut icon" href="wp-content/uploads/2013/10/AF.png" /><link>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width">
<link rel="stylesheet" href="playerhtml5.css">
<meta http-equiv="Content-Language" content="it" />
<meta name="author" content="EasyDeath&#174;">
<meta name="skype" content="easydeathyura">
<meta name="mail" content="death94@hotmail.it">
<link rel="stylesheet" type="text/css" href="0174/ds.css" />

<script type='text/javascript' src='//sadbads.com/83/76/de/8376de1dd13d9aa0a42c8725afb7847e.js'></script>


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">\$(document).ready(function(){\$('.lightsoff').append('<div class="lightsoffbtn" style="text-align: center; padding: 10px 0px 0px 0px; font-size:12pt; font-weight: bold;"><a href="#">Modalit&agrave; Cinema!</a></div>');\$('body').append('<div class="lightsoff-ovelay off" style="position:relative; z-index:1; display:none;">close</div>');var \$overlay=\$('.lightsoff-ovelay'),\$containers=\$('.lightsoff'),\$lightsoffTrigger=\$('.lightsoffbtn a');\$lightsoffTrigger.each(function(){var \$container=\$(this).parent().parent();\$container.css({'position':'relative'});\$(this).click(function(e){e.preventDefault();if(\$overlay.hasClass('off')){\$container.css({'z-index':4000});\$overlay.css({'position':'fixed','display':'block','text-indent':-99999,'background-color':'#000000','width':'100%','height':'100%','top':0,'left':0,'z-index':3000,'opacity':1,'cursor':'pointer'});\$overlay.removeClass('off').addClass('on').fadeIn();}else if(\$overlay.hasClass('on')){e.preventDefault();\$containers.css({'z-index':0});\$overlay.removeClass('on').addClass('off').fadeOut();}});});\$overlay.click(function(e){e.preventDefault();\$containers.css({'z-index':0});\$overlay.removeClass('on').addClass('off').fadeOut();});});</script>
</head>

<body>
<center>
<a href="/" target="_blank"><img src="wp-content/uploads/2013/05/logo-animeforce.png" alt="AnimeForce" title="AnimeForce"></a>
<div id="wtf" style="max-width: 950px;">
<style>#headerdivfixed{height:90px;background-color:transparent;overflow:hidden}.intrinsic-container{position:relative;height:0;overflow:hidden}.intrinsic-container-16x9{padding-bottom:56.25%}.intrinsic-container-4x3{padding-bottom:75%}.intrinsic-container iframe{position:absolute;top:0;left:0;width:100%;height:100%}</style>
<div id="headerdivfixed">

</div>
</div>

<div id="wtf" class="button">
<a href="$url" target="_blank">Download</a>

<a href="ds16.php?file=www.lacumpa.org/DDL/ANIME/25-saiNoJoshikousei/25-saiNoJoshikousei_Ep_01_SUB_ITA.mp4&s=alt">Streaming Alternativo</a>
</div>
<div id="wtf" class="html5video lightsoff" style="width:100%;">
<div id="wtf" class="main-container">
<div class="hero-unit">
<video id='video-player' preload='metadata' controls>
<source src="$url" type="video/mp4">
</video>
</div>
</div>
</div>





</center>
<footer>
<center>
<div id="fb-root"></div>
<script>(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="//connect.facebook.net/it_IT/all.js#xfbml=1&appId=1416172718605371";fjs.parentNode.insertBefore(js,fjs);}(document,'script','facebook-jssdk'));</script>
<div id="buttons" style="padding-top: 8px;">
<div class="fb-like" data-href="https://www.facebook.com/animeforceteam" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
</div>
<br><br>
<div id="wtf" style="max-width: 950px;">
<style>.inline{display:inline-block;border:0 solid #000;margin:5px}#footerdivfixed{width:90%;height:auto;background-color:transparent;overflow:hidden}</style>
<div>
<div class='inline'><div id="footerdivfixed">

<div id="rn_ad_native_sj8f7"></div>
<script src="//cdn.run-syndicate.com/sdk/v1/n.js"></script>
<script>NativeAd({element_id:"rn_ad_native_sj8f7",spot:"bcd4f9cbdd6e48a9be5daf2d644611c1",type:"label-under",cols:4,rows:1,title:"",titlePosition:"left",adsByPosition:"right",keywords:"{ keywords }"});</script>

</div></div>

</div>
</div>
</center>

<script type="text/javascript">if(!window.BB_ind){BB_ind=0;}if(!window.BB_r){BB_r=Math.floor(Math.random()*1000000000)}BB_ind++;window.BB_skin={centerWidth:910,centerDomId:'',leftOffset:0,rightOffset:0,topPos:0,deferLoading:false,fixed:true,fixedStickTopOnScroll:false,fixedScrollSecondaryTop:0,adjustSkinOnDynamicCenter:true,zIndex:0,leftFrameId:'',rightFrameId:'',pl:41149,index:BB_ind};</script>
<script type="text/javascript" async src="//st.bebi.com/bebi_v3.js"></script>


<script type="text/javascript">document.write(unescape("%3Cscript src=%27http://s10.histats.com/js15.js%27 type=%27text/javascript%27%3E%3C/script%3E"));</script>
<a href="//www.histats.com" target="_blank" title="contatore utenti connessi"><script type="text/javascript">try{Histats.start(1,2004294,4,0,0,0,"");Histats.track_hits();}catch(err){};</script></a>
<noscript><a href="//www.histats.com" target="_blank"><img src="//sstatic1.histats.com/0.gif?2004294&101" alt="contatore utenti connessi" border="0"></a></noscript>

<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create','UA-68140415-1','auto');ga('send','pageview');</script>
</footer>
</body>
</html> 
  ''';
}
