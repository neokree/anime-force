import 'dart:io';

import 'package:anime_force/src/core/component/anime/domain/website.dart';
import 'package:mock_web_server/mock_web_server.dart';

import '../../test/fixture/anime.dart';
import '../../test/framework/page_builder.dart';

const defaultServerPort = 4444;

/// Fake version of the real website
class FakeAnimeForceWebsite {

  final MockWebServer _server;
  PageBuilder pageBuilder;
  Uri host;

  FakeAnimeForceWebsite(this.host) : _server = MockWebServer(port: host.port);

  start() async {
    await _server.start();

    pageBuilder = PageBuilder(host);

    _server.dispatcher = (HttpRequest request) async {

      /// Anime list page
      if (request.uri.path == '/lista-anime/') {
        String html = pageBuilder.buildAnimeListPage(animeLinks);

        return MockResponse()
          ..httpCode = 200
          ..body = html;
      }

      /// Redirect page
      if (request.uri.path == '/ds.php' &&
          request.uri.queryParameters['file'] != null) {
        StreamingPage episode = _episodeByDsPath(request.uri.queryParameters['file']);

        if (episode != null) {
          return MockResponse()
            ..httpCode = 301
            ..headers = {
              'Location':
                  Uri.parse('/ds16.php?file=${episode.source}').toString()
            };
        }
      }

      /// Streaming page
      if (request.uri.path == '/ds16.php' &&
          request.uri.queryParameters['file'] != null) {
        var episode = _episodeByDs16Path(request.uri.queryParameters['file']);

        if (episode != null) {
          String htmlPage = pageBuilder.buildEpisodeVideoPage(episode);
          return MockResponse()
            ..httpCode = 200
            ..body = htmlPage;
        }
      }

      try {
        /// Anime detail page
        var animeDetail = animeDetailMap[request.uri.path];
        String html = pageBuilder.buildAnimeDetailPage(animeDetail);
        return MockResponse()
          ..httpCode = 200
          ..body = html;
      } catch (e) {
        return MockResponse()
          ..body = "Path does not exist!"
          ..httpCode = 404;
      }
    };
  }

  StreamingPage _episodeByDsPath(String fileQuery) {
    EpisodeLink link =
        animeDetailMap.values.fold(List<EpisodeLink>(), (episodes, anime) {
      episodes.addAll(anime.episodes);
      return episodes;
    }).firstWhere((episode) {
      return episode.uri.toString().contains(fileQuery);
    }, orElse: () => null);
    return streamingByEpisodeLink[link];
  }

  _episodeByDs16Path(String fileQuery) {
    StreamingPage episode = streamingByEpisodeLink.values.firstWhere((episode) {
      return episode.source.toString().contains(fileQuery);
    }, orElse: () => null);
    return episode;
  }

  shutdown() {
    _server.shutdown();
  }
}
