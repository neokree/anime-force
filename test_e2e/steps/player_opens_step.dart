import 'package:flutter_driver/flutter_driver.dart';
import 'package:gherkin/gherkin.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';

/// Flutter cannot check if the app opens another intent,
/// so we check if the page is NOT triggering an error
class ThenIExpectThePlayerOpens extends ThenWithWorld<FlutterWorld> {
  @override
  RegExp get pattern => RegExp(r'I expect the player will open');

  @override
  Future<void> executeStep() async {
    bool absent = await FlutterDriverUtils.isAbsent(
      world.driver,
      find.byValueKey('opening error'),
    );
    expect(absent, true);
  }
}
