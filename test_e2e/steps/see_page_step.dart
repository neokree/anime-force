import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

class GivenISeePage extends Given1WithWorld<String, FlutterWorld> {

  @override
  RegExp get pattern => RegExp(r"I see the {String} page");

  @override
  Future<void> executeStep(String pageName) async {
    final key = find.byValueKey(pageName);
    bool found = await FlutterDriverUtils.isPresent(key, world.driver);
    expect(found, true, reason: 'Page $pageName not found');
  }

}