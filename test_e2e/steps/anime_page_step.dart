import 'package:flutter_driver/flutter_driver.dart';
import 'package:gherkin/gherkin.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';

class GivenIamOnAnimePage extends Given1WithWorld<String, FlutterWorld> {
  @override
  RegExp get pattern => RegExp(r'I am on {string} anime page');

  @override
  Future<void> executeStep(String animeName) async {
    await FlutterDriverUtils.tap(world.driver, find.text(animeName));
    expect(await FlutterDriverUtils.isPresent(find.byValueKey('Anime Detail'), world.driver), true);
    expect(await FlutterDriverUtils.isPresent(find.byValueKey('name'), world.driver), true);
  }
}