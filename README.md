# anime_force

The unofficial mobile app of Anime Force

## Goal

The goal of this application is to be able to perform all operations that 
are available on [anime force website](https://ww1.animeforce.org/), 
using a mobile friendly app without ads.

## Architecture

The architecture of this Flutter application follow the Explicit Architecture 
guidelines from [@herbertograca](https://herbertograca.com/).

See blog post [#1](https://herbertograca.com/2017/11/16/explicit-architecture-01-ddd-hexagonal-onion-clean-cqrs-how-i-put-it-all-together/),
[#2](https://herbertograca.com/2018/07/07/more-than-concentric-layers/) and 
[#3](https://herbertograca.com/2019/06/05/reflecting-architecture-and-domain-in-code/) 
for an introduction.


